# About
Robot code for FRC 246's Deep Space robot.


Licensed under the GPLv3. Though you are not obligated to release your modifications, as FRC is not a commercial use, it is highly encouraged that you do so to be in accord with the spirit of the license.

