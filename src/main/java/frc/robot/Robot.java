/*----------------------------------------------------------------------------*/
/* Copyright (c) 2017-2018 FIRST. All Rights Reserved.                        */
/* Open Source Software - may be modified and shared by FRC teams. The code   */
/* must be accompanied by the FIRST BSD license file in the root directory of */
/* the project.                                                               */
/*----------------------------------------------------------------------------*/

package frc.robot;

import edu.wpi.first.wpilibj.TimedRobot;
import edu.wpi.first.wpilibj.smartdashboard.SendableChooser;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import frc.robot.subsystems.drivetrain.Drivetrain;

import java.sql.Driver;

import edu.wpi.first.networktables.NetworkTable;
import edu.wpi.first.networktables.NetworkTableEntry;
import edu.wpi.first.networktables.*;

import edu.wpi.first.wpilibj.DoubleSolenoid;
import edu.wpi.first.wpilibj.GenericHID.Hand;

import frc.robot.subsystems.intake.Intake;
import frc.robot.subsystems.claw.Claw;
import frc.robot.subsystems.claw.Claw.ClawMode;

import frc.robot.subsystems.lift.Lift;
import frc.robot.subsystems.lift.Lift.LiftMode;
import frc.robot.subsystems.pivotarm.PivotArm;
import frc.robot.subsystems.pivotarm.PivotArm.PivotMode;
import frc.robot.subsystems.pivotarm.PivotArm;
import edu.wpi.first.wpilibj.XboxController;
import frc.robot.olibs.util.DAController;
import frc.robot.olibs.util.DualAction;
import edu.wpi.first.wpilibj.DoubleSolenoid.Value;

/**
 * The VM is configured to automatically run this class, and to call the
 * functions corresponding to each mode, as described in the IterativeRobot
 * documentation. If you change the name of this class or the package after
 * creating this project, you must also update the build.gradle file in the
 * project.
 */


public class Robot extends TimedRobot {
	private static final String kDefaultAuto = "Default";
	private static final String kCustomAuto = "My Auto";
	private String m_autoSelected;
	private final SendableChooser<String> m_chooser = new SendableChooser<>();
	public Drivetrain drivetrain;

	public Intake intake;
	
	//public Claw claw;
	public PivotArm pivot;
	public Lift lift;

	public NetworkTableInstance instance;

	public NetworkTable limelight;

	public NetworkTableEntry limelightMode, lumen;
	
	public DAController drive1;
	public DAController operator1;
	public DoubleSolenoid shifter;
	public boolean temp = false;


	boolean inIllegalPos;
	/**
	 * This function is run when the robot is first started up and should be
	 * used for any initialization code.
	 */
	@Override
	public void robotInit() {
		m_chooser.setDefaultOption("Default Auto", kDefaultAuto);
		m_chooser.addOption("My Auto", kCustomAuto);
		SmartDashboard.putData("Auto choices", m_chooser);
		
		drivetrain = new Drivetrain();
		
		shifter = new DoubleSolenoid(22, 0, 1);
		
		lift = new Lift();
		//claw = new Claw();
		intake = new Intake();
		pivot = new PivotArm();
		
		
		instance = NetworkTableInstance.getDefault();

		limelight = instance.getTable("limelight");
		
		drive1 = new DAController(0);
		operator1 = new DAController(1);
		

	}

	/**
	 * This function is called every robot packet, no matter the mode. Use
	 * this for items like diagnostics that you want ran during disabled,
	 * autonomous, teleoperated and test.
	 *
	 * <p>This runs after the mode specific periodic functions, but before
	 * LiveWindow and SmartDashboard integrated updating.
	 */
	@Override
	public void robotPeriodic() {

		//Limelight
	//	limelight.getEntry("camMode").setDouble(1.0);
	//	limelight.getEntry("ledMode").setDouble(1.0);

		//Shifting
		if(drive1.getRawButton(DualAction.A)) {
			shifter.set(Value.kForward);
		} else if (drive1.getRawButton(DualAction.B)) {
			shifter.set(Value.kReverse);
		}
		
	
		//Drive Control
		double RyAxis = drive1.getRawAxis(DualAction.RightXAxis); 
		double LxAxis = drive1.getRawAxis(DualAction.LeftYAxis);
		drivetrain.openLoopDrive(-1*LxAxis, RyAxis);
		//double ForwardSpeed= drive1.getRawAxis(DualAction.LeftYAxis);
		//double TurnSpeed= drive1.getRawAxis(DualAction.RightXAxis);
		//drivetrain.tankDrive(ForwardSpeed+TurnSpeed/5,ForwardSpeed-TurnSpeed/5);

		

		//Pivot Control
		if(operator1.getRawButton(DualAction.X)){
		//	pivot.manualPivot(0.5);
		} if(operator1.getRawButton(DualAction.Y)){
		//	pivot.manualPivot(-0.5);

		} else {
		///	pivot.manualPivot(0);
		}

		//if(operator1.getRawButton(DualAction.LeftTrigger)){pivot.pivotSet(PivotMode.Down);}
		//if(operator1.getRawButton(DualAction.RightTrigger)){pivot.pivotSet(PivotMode.Up);}

		//Claw Control
		//if(operator1.getRawButton(DualAction.LeftBumper)){claw.clawSet(ClawMode.Down);}
		//if(operator1.getRawButton(DualAction.RightBumper)){claw.clawSet(ClawMode.Up);}

		//Intake Control
		//if(drive1.getRawButton(DualAction.LeftTrigger)){intake.takeInBall(1);}
		//if(drive1.getRawButton(DualAction.RightTrigger)){intake.ejectBall(1);}

		//Lift Control
	/*	for(int i = 0; i < PivotArm.illegalPositions.length; i++)
		{
			if(pivot.pivotPos == PivotArm.illegalPositions[i])
			{
				inIllegalPos = true;
			}
		}
			if(operator1.getRawButton(DualAction.A))
			{
		//		if(operator1.getPOV(0) == 270){lift.setHeight(LiftMode.RocketCargo, 0, inIllegalPos);}
		//		if(operator1.getPOV(0) == 360){lift.setHeight(LiftMode.RocketCargo, 1, inIllegalPos);}
		//		if(operator1.getPOV(0) == 90){lift.setHeight(LiftMode.RocketCargo, 2, inIllegalPos);}
			}

			if(operator1.getRawButton(DualAction.B))
			{
		//		if(operator1.getPOV(0) == 270){lift.setHeight(LiftMode.RocketHatch, 0, inIllegalPos);}
		//		if(operator1.getPOV(0) == 360){lift.setHeight(LiftMode.RocketHatch, 1, inIllegalPos);}
		//		if(operator1.getPOV(0) == 90){lift.setHeight(LiftMode.RocketHatch, 2, inIllegalPos);}
			}

		if(operator1.getRawButton(DualAction.X))
		{
		//	lift.setHeight(LiftMode.CSCargo, 0, inIllegalPos);
		}

		if(operator1.getRawButton(DualAction.Y))
		{
		//	lift.setHeight(LiftMode.CSHatch, 0, inIllegalPos);
		}*/
	}

	/**
	 * This autonomous (along with the chooser code above) shows how to select
	 * between different autonomous modes using the dashboard. The sendable
	 * chooser code works with the Java SmartDashboard. If you prefer the
	 * LabVIEW Dashboard, remove all of the chooser code and uncomment the
	 * getString line to get the auto name from the text box below the Gyro
	 *
	 * <p>You can add additional auto modes by adding additional comparisons to
	 * the switch structure below with additional strings. If using the
	 * SendableChooser make sure to add them to the chooser code above as well.
	 */
	@Override
	public void autonomousInit() {
		m_autoSelected = m_chooser.getSelected();
		// autoSelected = SmartDashboard.getString("Auto Selector",
		// defaultAuto);
		System.out.println("Auto selected: " + m_autoSelected);
	}

	/**
	 * This function is called periodically during autonomous.
	 */
	@Override
	public void autonomousPeriodic() {
//		drivetrain.openLoopDrive(drive1.getY(Hand.kLeft),-1*drive1.getX(Hand.kRight));
		//drivetrain.openLoopDrive(-1 * drive1.getRawAxis(1), drive1.getRawAxis(4));

}

	/**
	 * This function is called periodically during operator control.
	 */
	@Override
	public void teleopPeriodic() {

	}
	/**
	 * This function is called periodically during test mode.
	 */
	@Override
	public void testPeriodic() {

	}

}
