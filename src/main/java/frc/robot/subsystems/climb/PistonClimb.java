package frc.robot.subsystems.climb;

import edu.wpi.first.wpilibj.DoubleSolenoid;
import edu.wpi.first.wpilibj.DoubleSolenoid.Value;
import edu.wpi.first.wpilibj.command.Subsystem;

public class PistonClimb extends Climb 
{

	private final DoubleSolenoid[] dsList;

	public PistonClimb (DoubleSolenoid[] dsList)
	{
		this.dsList = dsList;	
	}

	/*
	 * Descriptive aliases for the more primitive dsList acessors
	 */

	public void extendPistons(){
		dsListSet(Value.kForward);
	}

	public void recallPistons(){
		dsListSet(Value.kReverse);
	}

	public void nullPistons(){
		dsListSet(Value.kOff);
	}

	/*
	 * Getters and setters for the DoubleSolenoid array which is used by the Climb class
	 * Sets the Value enum which has the values kOff, kForward, and kReverse
	 */

	private void dsListSet (DoubleSolenoid.Value value) {
		for(short i = 0; i < this.dsList.length; i++){
			this.dsList[i].set(value);
		}
	}

	private DoubleSolenoid.Value[] dsListGet(){
		DoubleSolenoid.Value[] dsListVals;
		dsListVals = new DoubleSolenoid.Value[this.dsList.length];
		for(short i = 0; i < this.dsList.length; i++){
			dsListVals[i] = this.dsList[i].get();
		}
		return dsListVals;
	}

}
