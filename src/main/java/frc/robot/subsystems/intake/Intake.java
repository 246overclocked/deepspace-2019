
package frc.robot.subsystems.intake;
import edu.wpi.first.wpilibj.command.Subsystem;
import com.ctre.phoenix.motorcontrol.can.TalonSRX;
import com.ctre.phoenix.motorcontrol.ControlMode;
import edu.wpi.first.wpilibj.AnalogInput;
import edu.wpi.first.wpilibj.DoubleSolenoid;
import edu.wpi.first.wpilibj.DoubleSolenoid.Value;
public class Intake extends Subsystem
{
    
    private static int solenoid1 = 2;
    private static int solenoid2 = 3;
    private static int PCMId = 22;

    
    
    private static int talonPort;

    private TalonSRX wheels;
    private DoubleSolenoid popSol;
    
    public Intake(){
        this.wheels = new TalonSRX(talonPort);
        this.popSol = new DoubleSolenoid(PCMId, solenoid1, solenoid2);
    }
    
    public void ejectBall(double ejectSpeed){
        this.wheels.setInverted(false);
        this.wheels.set(ControlMode.PercentOutput, ejectSpeed);
    }
    
    public void takeInBall(double takeSpeed){
        this.wheels.setInverted(true);
        this.wheels.set(ControlMode.PercentOutput, takeSpeed);
    }
    public void voltageControl(double speed){
        //yes that name is a placeholder
        this.wheels.setInverted(false);
        this.wheels.set(ControlMode.PercentOutput, speed);
    }
    
    //The top two are needed if it is two buttons controlling the ball ejection and intake
    //The third is needed if it is a toggle thing
    
    public Value getPop(){
        Value solenoidValue = this.popSol.get();
        return solenoidValue;
    }
    
    public void setPop(Value desiredGear){
        this.popSol.set(desiredGear);
    }
    
    @Override
    public void initDefaultCommand(){
        
    }
}