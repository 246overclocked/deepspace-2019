package frc.robot.subsystems.lift;
import com.ctre.phoenix.motorcontrol.can.WPI_TalonSRX;
import com.ctre.phoenix.motorcontrol.ControlMode;
import com.ctre.phoenix.motorcontrol.FeedbackDevice;
import com.ctre.phoenix.motorcontrol.NeutralMode;
import edu.wpi.first.wpilibj.SpeedControllerGroup;
import edu.wpi.first.wpilibj.command.Subsystem;
import com.ctre.phoenix.motorcontrol.can.*;
import java.lang.Math;

public class Lift extends Subsystem 
{
	// Hardware
	private WPI_TalonSRX[] leftMotors, rightMotors;

	private static final int[] leftMotorIds = {5, 9};
	private static final int[] rightMotorIds = {4, 8};
	TalonSRX leftMaster, rightMaster, leftSlave, rightSlave;

	private SpeedControllerGroup left, right;

	private static final double[] pidConstants = {0.5, 0.5, 0.5, 0.5}; //kF, kP, kD, kI
	

	private static final double radius = 2;
	private static final double inchesPerRotation = 2 * Math.PI * radius;

	private static final double baseHeight = 0;

	private static final double ticksPerInch = 1;

	private static double liftPos = 0;

	private static final double[] illegalPositions = {};
	
	public static final double[][] liftHeights = {{27.5, 55.5, 83.5}, {19, 47, 75}, {18.5}, {30}}; //cargo, hatch, cargoship{hatch, ball}

	public enum LiftMode
	{
		RocketCargo(0),
		RocketHatch(1),
		CSHatch(2),
		CSCargo(3);

		private final int value;
		private LiftMode(int value)
		{
			this.value = value;
		}
		
		public int getValue()
		{
			return this.value;
		}
	}
	
	public void initDefaultCommand ()
	{
	}
	 //Ignore
	public Lift(){	

		leftMaster= new TalonSRX(5);
		leftMaster.setInverted(true);
		leftSlave = new TalonSRX(9);
		leftSlave.setInverted(true);		
		rightMaster = new TalonSRX(8);
		rightSlave = new TalonSRX(4);

		leftSlave.follow(leftMaster);
		rightSlave.follow(rightMaster);

		leftMaster.setSensorPhase(true);
		rightMaster.setSensorPhase(true);

		leftMaster.configSelectedFeedbackSensor(FeedbackDevice.CTRE_MagEncoder_Absolute, 0, 10);
		rightMaster.configSelectedFeedbackSensor(FeedbackDevice.CTRE_MagEncoder_Absolute, 0, 10);

		leftMaster.config_kF(0, pidConstants[0], 10);
		leftMaster.config_kP(0, pidConstants[1], 10);
		leftMaster.config_kD(0, pidConstants[2], 10);
		leftMaster.config_kI(0, pidConstants[3], 10);

		rightMaster.config_kF(0, pidConstants[0], 10);
		rightMaster.config_kP(0, pidConstants[1], 10);
		rightMaster.config_kD(0, pidConstants[2], 10);
		rightMaster.config_kI(0, pidConstants[3], 10);

		leftMaster.setSelectedSensorPosition(0, 0, 10);
		rightMaster.setSelectedSensorPosition(0, 0, 10);
		liftPos = baseHeight;
	}

    /** 
     * This function takes a given target opening on the rocket and positions the lift at the nessasary height.
     * @param face is 0 for one of the sides of the rockets, or 1 for the middle of the rocket
     * @param level is 0 for the lowest level of the rocket, 1 for the middle, or 2 for the top of the rocket
     */

	public void setHeight(LiftMode mode, int height, boolean illegal)
	{
		liftPos = liftHeights[mode.getValue()][height];
		boolean liftIllegal = false;
		for(int i = 0; i < illegalPositions.length; i++)
		{
			if(liftPos == illegalPositions[i])
			{
				liftIllegal = true;
			}
		}
		if(liftIllegal == false || illegal == false)
		{
			double heightInTicks = (liftHeights[mode.getValue()][height] - baseHeight) * ticksPerInch; //TicksPerInch must be adjusted
			leftMaster.set(ControlMode.Position, heightInTicks); 
			rightMaster.set(ControlMode.Position, heightInTicks);
		}
	}
	public void manualControl(double velocity){
		leftMaster.set(ControlMode.PercentOutput, velocity);
		rightMaster.set(ControlMode.PercentOutput, velocity);
	}
	void manualLife(){


	}
}
