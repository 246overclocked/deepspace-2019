package frc.robot.subsystems.drivetrain;

import com.ctre.phoenix.motorcontrol.ControlMode;
import com.ctre.phoenix.motorcontrol.can.*;
import edu.wpi.first.wpilibj.SpeedControllerGroup;
import edu.wpi.first.wpilibj.command.Subsystem;
import edu.wpi.first.wpilibj.drive.DifferentialDrive;


public class Drivetrain extends Subsystem
{
//When moving forward and back motors are missmatched in power turns to right
//Can't turn while in the same spot to to the right (maybe the left too)

	private WPI_TalonSRX leftMaster, rightMaster, rightSlave, leftSlave;
	private DifferentialDrive ddrive;
	private SpeedControllerGroup left, right;
	
	public Drivetrain ()
	{
		leftMaster = new WPI_TalonSRX(9);
		
		rightMaster = new WPI_TalonSRX(6);
		leftSlave = new WPI_TalonSRX(1);
		leftSlave.setInverted(true);
		rightSlave = new WPI_TalonSRX(8);

		left = new SpeedControllerGroup(leftMaster, leftSlave);
		left.setInverted(true);
		right = new SpeedControllerGroup(rightMaster, rightSlave);
		ddrive = new DifferentialDrive(left, right);
	}

	public void openLoopDrive (double throttle, double rotation)
	{
		ddrive.arcadeDrive(throttle, rotation);
	}

-
	@Override
	public void initDefaultCommand () { }
}
