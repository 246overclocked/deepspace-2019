package frc.robot.subsystems.pivotarm;
import edu.wpi.first.wpilibj.command.Subsystem;
import com.ctre.phoenix.motorcontrol.can.TalonSRX;
import com.ctre.phoenix.motorcontrol.ControlMode;
import com.ctre.phoenix.motorcontrol.FeedbackDevice;
import com.ctre.phoenix.motorcontrol.NeutralMode;
import java.lang.Math;

public class PivotArm extends Subsystem
{

    public TalonSRX pivotMotorArm;
    public TalonSRX pivotMotorIntake;


    private static double[] pivotPIDArm = {0, 0, 0, 0}; // kF, kP, kD, kI
    private static double[] pivotPIDIntake = {0, 0, 0, 0}; // kF, kP, kD, kI


    public static double positions[] = {0, 1/4};

    public static double illegalPositionsArm[] = {};
    public static double illegalPositionsIntake[] = {};


    public enum PivotMode
    {
        Down(0),
        Up(1);


        private int value;
        private PivotMode(int value)
        {
            this.value = value;
        }

        public int getValue()
        {
            return this.value;
        }
    }

    public double pivotPosArm;
    public double pivotPosIntake;

    
    private static int armTalonId = 50;
    private static int intakeTalonId = 50;


    public PivotArm()
    {
        pivotMotorArm = new TalonSRX(armTalonId);

        pivotMotorArm.setSensorPhase(true);
        pivotMotorArm.configSelectedFeedbackSensor(FeedbackDevice.CTRE_MagEncoder_Absolute, 0, 10);
        pivotMotorArm.setNeutralMode(NeutralMode.Brake);

        pivotMotorArm.config_kF(0, pivotPIDArm[0], 10);
        pivotMotorArm.config_kP(0, pivotPIDArm[1], 10);
        pivotMotorArm.config_kD(0, pivotPIDArm[2], 10);
        pivotMotorArm.config_kI(0, pivotPIDArm[3], 10);
        
        pivotMotorArm.setSelectedSensorPosition(0);
        pivotPosArm = 0;
        pivotPosIntake = 0;
    }

    public void pivotSet(PivotMode armPosition,PivotMode intakePosition)
    {
        pivotMotorArm.set(ControlMode.Position, positions[armPosition.getValue()] * 4096);
        pivotMotorIntake.set(ControlMode.Position, positions[intakePosition.getValue()] * 4096);
        
        pivotPosArm = armPosition.getValue();
        pivotPosIntake = intakePosition.getValue();
    }
    public void manualPivot(double velocity)
    {
        pivotMotorArm.set(ControlMode.PercentOutput, velocity/10);
        pivotMotorIntake.set(ControlMode.PercentOutput, velocity/7);
    }    
    @Override
    public void initDefaultCommand(){
        
    }
}