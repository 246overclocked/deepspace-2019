package frc.robot.subsystems.claw;
import edu.wpi.first.wpilibj.command.Subsystem;
import com.ctre.phoenix.motorcontrol.can.TalonSRX;
import com.ctre.phoenix.motorcontrol.ControlMode;
import com.ctre.phoenix.motorcontrol.FeedbackDevice;
import com.ctre.phoenix.motorcontrol.NeutralMode;

import java.lang.Math;
public class Claw extends Subsystem{

    public TalonSRX clawMotor;

    private static double[] clawPID = {0, 0, 0, 0}; // kF, kP, kD, kI

    public static double positions[] = {0, 1/4};

    public double cPos;
    
    private static int talonId = 200;

    public enum ClawMode
    {
        Down(0),
        Up(1);
        private int value;
        private ClawMode(int value)
        {
            this.value = value;
        }

        public int getValue()
        {
            return this.value;
        }
    }

    public Claw()
    {
        clawMotor = new TalonSRX(talonId);

        clawMotor.setSensorPhase(true);
        clawMotor.configSelectedFeedbackSensor(FeedbackDevice.CTRE_MagEncoder_Relative, 0, 10);
        clawMotor.setNeutralMode(NeutralMode.Brake);

        clawMotor.config_kF(0, clawPID[0], 10);
        clawMotor.config_kP(0, clawPID[1], 10);
        clawMotor.config_kD(0, clawPID[2], 10);
        clawMotor.config_kI(0, clawPID[3], 10);
        
        clawMotor.setSelectedSensorPosition(0);
        cPos = 0;
    }

    public void clawSet(ClawMode position)
    {
        clawMotor.set(ControlMode.Position, positions[position.getValue()] * 4096);
        cPos = positions[position.getValue()];
    }
    public void clawManual(double velocity)
    {
        clawMotor.set(ControlMode.PercentOutput, velocity);
    }    
    @Override
    public void initDefaultCommand(){
        
    }
}