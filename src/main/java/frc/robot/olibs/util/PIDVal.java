package frc.robot.olibs.util;
public class PIDVal
{
	public double kF, kP, kD, kI;	

	public PIDVal(double kF, double kP, double kD, double kI){
		this.kF = kF;
		this.kP = kP;
		this.kD = kD;
		this.kI = kI;
	}
}
