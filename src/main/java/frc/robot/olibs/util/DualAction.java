package frc.robot.olibs.util;
public enum DualAction
{
    LeftXAxis(0),
    LeftYAxis(1),
    RightXAxis(2),
    RightYAxis(3),
    DPadXAxis(4),
    DPadYAxis(5),
    X(0),
    A(1),
    B(2),
    Y(3),
    LeftBumper(4),
    RightBumper(5),
    LeftTrigger(6),
    RightTrigger(7),
    Back(8),
    Start(9),
    StickButtonLeft(10),
    StickButtonRight(11);
    //POV:
    //TL A4=0, A5=0
    //T A4=0.5, A5=0
    //TR A4=1, A5=0
    //R A4=1, A5=0.5
    //BR A4=1, A5=1
    //B A4=0.5, A5=1
    //BL A4=0, A5=1
    //L A4=0, A5=0.5 


    private int value;
    private DualAction(int value)
    {
        this.value = value;
    }

    public int getValue()
    {
        return this.value;
    }
}