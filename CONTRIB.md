# Style

This project uses the GNU Java style guide, which can be found [here](https://www.gnu.org/software/gnu-crypto/code-formatting.html), with several exceptions.

You shall not use 2 spaces but 4 space tabs.

For loops shall not be formatted like this:
```java
  for (int i = 0; i < limit; i++)
      {
	      statements;
      }
``` 

but like this:
```java
  for (int i = 0; i < limit; i++)
  {
      statements;
  }
```
Method names `areLikeThis`.

Field/variable names `areLikeThis`.

Class names `AreLikeThis`.

# Rules

If your patch includes any sort of IntelliJ or other build/config files, it will be rejected. Please get rid of those in your .gitignore.
